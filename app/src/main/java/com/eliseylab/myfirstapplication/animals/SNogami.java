package com.eliseylab.myfirstapplication.animals;

/*
 * This file is part of My first application.
 *
 * Created by Artem Kuznetsov on 10/03/2021
 *
 * (c) 2021 November Five BVBA
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
abstract class SNogami implements Animal {
	@Override
	public Integer getLegs() {
		return 4;
	}
}
