package com.eliseylab.myfirstapplication.animals;

import android.util.Log;

// Кот являет ребенком от Animal. Animal - interface, который не имеет сам никакой логики. Он описал, что
// животное имеет лапы и может сделать какой-то звук.
// Ниже есть сам код для описания этих методов.
class Cat extends SNogami {

	@Override
	public void makeSomeNoise() {
		Log.d("Animal", "This is cat and it makes some MauMau");
	}
}
