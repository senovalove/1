package com.eliseylab.myfirstapplication.animals;


// Interface не может иметь никаких методов сам. Это лишь описание обьекта.
// Его нельзя extend - унаследовать. Его можно лишь implement, то есть сделать логику для него
// В случае с extend не обязательно перезаписывать все методы в дочернем элементе.
// А случае с implement - ты обязан написать код для этих методов поскольку interface их не имеет.
interface Animal {
	Integer getLegs();

	void makeSomeNoise();
}
