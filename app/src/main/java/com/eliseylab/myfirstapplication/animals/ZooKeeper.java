package com.eliseylab.myfirstapplication.animals;

import android.util.Log;

import java.util.ArrayList;


class ZooKeeper {
	// Собакен и Кот - животные. Поэтому тип данного списка - Животное.
	final ArrayList<Animal> zoo = new ArrayList<Animal>();

	public ZooKeeper() {
		Log.d("ZooKeeper", "Object is created");
	}

	public void populateZoo() {
		zoo.add(new Dog());
		zoo.add(new Cat());
	}

	// Заставляет каждое животное сделать звук.
	public void letThemMakeNoise() {
		for (Animal animal : zoo) {
			animal.makeSomeNoise();
		}
	}

	public Integer homManyLegsAreInThisZoo() {
		Integer legs = 0;
		for (Animal animal : zoo) {
			legs += animal.getLegs();
		}
		return legs;
	}
}
