package com.eliseylab.myfirstapplication.animals;

import android.util.Log;

// Собакен являет ребенком от Animal. Animal - interface, который не имеет сам никакой логики. Он описал, что
// животное имеет лапы и может сделать какой-то звук.
// Ниже есть сам код для описания этих методов.
class Dog extends SNogami {

	@Override
	public void makeSomeNoise() {
		Log.d("Animal", "This is dog and it makes some " + doWafWaf());
	}

	// Частный метод собакена. Не каждое животное может делать ваф, но каждое может делать звук.
	// Поэтому makeSomeNoise() находится в interface, а ВафВаф только у собакена
	private String doWafWaf() {
		return "WafWaf";
	}
}
