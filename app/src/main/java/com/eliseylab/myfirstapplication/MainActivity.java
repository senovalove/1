// Имя пакета для JVM. Jav'е не важен путь к файлу, не важно в какой папке находится код
// package заменяет это.
package com.eliseylab.myfirstapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

// extends - означает наследование;
// Поскольку это экран, твой класс наследует все от AppCompatActivity
// Если посмотришь, то AppCompatActivity extends FragmentActivity
// А FragmentActivity extends ComponentActivity
// А ComponentActivity extends Activity
// Таким образом твой класс является дочерним всех классов :top: выше
public class MainActivity extends AppCompatActivity {

	// @Override - флаг, означабщий, что ты перезаписываешь метод родителя.
	// Если нажмешь слева на синюю иконку сразу слева от protected, то увидишь метод родителя
	// Нам необходимо перезаписать этот метод, чтобы указать экрану
	// какой файл для интерфейса он должен использовать
	// onCreate() вызывается android в момент создания экрана
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// super. означает вызов метода родителя.
		// То есть ты говоришь, что при создании android должен вызвать метод родителя.
		// Родитель создает необходимое для создания элементов.
		super.onCreate(savedInstanceState);
		// Здесь ты указываешь, что вид твоего экрана нахоидтся в файле activity_main.
		// То есть родитель создал для тебя все необходимое строчкой выше, а здесь ты указываешь вид экрана
		setContentView(R.layout.activity_main);

		// R.layout.activity_main:
		// R - сокращение от Resources
		// layout - указывает, что ты хочешь использовать вид
		// activity_main - какой именно
		// Этот файл лежит у тебя в папке res/layout/activity_main.xml

		// setOnClickListener();
		final Button button = findViewById(R.id.myButton);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
			}
		});
	}
}